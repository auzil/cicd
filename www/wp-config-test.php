<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'db' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '(2 z*tbg~u?X;Rx,b J{bFsbMkB:{pS0?Rx!7%Z`(M~~klTraw_=-NW[89>rMj-)' );
define( 'SECURE_AUTH_KEY',  'B$ut F4Wlp@B>L%zORv2Y)LNExs&^.|mi-4.),izg:C~:h!4_=Rr(dZ_P`,UNLl6' );
define( 'LOGGED_IN_KEY',    'SEi^-~3)if!N}XFda?G{;XBaZnTD@}pDd)cuOyy:`>hC%>3K<!e%  UT)Q%c07Se' );
define( 'NONCE_KEY',        'd>L[+K2.]$S-3IbIntz^NXw(*)+RwQ$fMcq|JNnx^KO:[d7R`/w`<z*+LPr^`dtQ' );
define( 'AUTH_SALT',        'U-eCy!t{^A2HP~`h|p+OVzJ%O{RY/%ETjQazFo782cag=@U*r?dHxQ9OJow=Lw0M' );
define( 'SECURE_AUTH_SALT', '(_S^I@syC;57I5(Q~}G^/,$Md@lb1m-NLE</$^AhbyI~3a}xr4Cz!Udcmi3ZuDKV' );
define( 'LOGGED_IN_SALT',   'N lI?QI4r+{b)xE[W(Wu0FOy~1cMYYg35eY[K:MKGWAOQebP%Ggqz.2JK.us5)y1' );
define( 'NONCE_SALT',       'u$~l!e=%O[apdbjap~d57GVj}_g95a`- n40k.0t7sZ;KTu1nnuGit/*do!lZr=]' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );


/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';


define( 'WP_REDIS_DISABLED', true );
